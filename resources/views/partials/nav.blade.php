
    <!-- Fixed Navigation Starts -->
    <ul class="icon-menu d-none d-lg-block revealator-slideup revealator-once revealator-delay1">
        <li class="icon-box {{ ($title === "Kevin") ? 'active' : '' }}">
            <i class="fa fa-home"></i>
            <a href="/">
                <h2>Home</h2>
            </a>
        </li>
        <li class="icon-box {{ ($title === "About") ? 'active' : '' }}">
            <i class="fa fa-user"></i>
            <a href="/about">
                <h2>About</h2>
            </a>
        </li>
        <li class="icon-box {{ ($title === "Contact") ? 'active' : '' }}">
            <i class="fa fa-envelope-open"></i>
            <a href="/contact">
                <h2>Contact</h2>
            </a>
        </li>
        <li class="icon-box {{ ($title === "Blog") ? 'active' : '' }}">
            <i class="fa fa-comments"></i>
            <a href="/postblog">
                <h2>Blog</h2>
            </a>
        </li>
    </ul>
    <!-- Fixed Navigation Ends -->
    <!-- Mobile Menu Starts -->
    <nav role="navigation" class="d-block d-lg-none">
        <div id="menuToggle">
            <input type="checkbox" />
            <span></span>
            <span></span>
            <span></span>
            <ul class="list-unstyled" id="menu">
                <li class="{{ ($title === "Kevin") ? 'active' : '' }}"><a href="/"> <i class="fa fa-home"></i><span>Home</span></a></li>
                <li class="{{ ($title === "About") ? 'active' : '' }}"><a href="/about"><i class="fa fa-user"></i><span>About</span></a></li>
                <li class="{{ ($title === "Contact") ? 'active' : '' }}"><a href="/contact"><i class="fa fa-envelope-open"></i><span>Contact</span></a></li>
                <li class="{{ ($title === "Blog") ? 'active' : '' }}"><a href="/blog"><i class="fa fa-comments"></i><span>Blog</span></a></li>
            </ul>
        </div>
    </nav>
    <!-- Mobile Menu Ends -->